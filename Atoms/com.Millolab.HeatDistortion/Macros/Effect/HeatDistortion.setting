{
	Tools = ordered() {
		HeatDistortion = MacroOperator {
			CtrlWZoom = false,
			Inputs = ordered() {
				CommentsNest = Input { Value = 0, },
				Comments = Input { Value = "Macro by Emilio Sapia - Millolab\nhttps://emiliosapia.myportfolio.com", },
				Background = InstanceInput {
					SourceOp = "ChangeDepth1",
					Source = "Input",
				},
				Display = InstanceInput {
					SourceOp = "ChangeDepth1",
					Source = "Display",
					Default = 2,
				},
				Animation = InstanceInput {
					SourceOp = "ChangeDepth1",
					Source = "AnimationControl",
				},
				HeatDirection = InstanceInput {
					SourceOp = "ChangeDepth1",
					Source = "HeatDirection",
					DefaultX = 0.5,
					DefaultY = 0.5,
				},
				Density = InstanceInput {
					SourceOp = "ChangeDepth1",
					Source = "Speed",
					MinScale = 9.99999975e-05,
					Default = 5,
				},
				Blur = InstanceInput {
					SourceOp = "ChangeDepth1",
					Source = "AnimationControl",
					Name = "Blur Controls",
				},
				HeatBlurFilter = InstanceInput {
					SourceOp = "ChangeDepth1",
					Source = "HeatBlurFilter",
				},
				HeatBlurSize = InstanceInput {
					SourceOp = "ChangeDepth1",
					Source = "HeatBlurSize",
					Default = 10,
				},
				Displace = InstanceInput {
					SourceOp = "ChangeDepth1",
					Source = "AnimationControl",
					Name = "Distortion  Controls",
				},
				Distortion = InstanceInput {
					SourceOp = "ChangeDepth1",
					Source = "Displace",
					Name = "Distortion",
					Default = 5,
				},
				Spread = InstanceInput {
					SourceOp = "ChangeDepth1",
					Source = "Spread",
					Default = 2.5,
				},
				BlurDetail = InstanceInput {
					SourceOp = "BlurNoise",
					Source = "Detail",
					Page = "Blur Noise",
					Default = 5.28,
				},
				BlurContrast = InstanceInput {
					SourceOp = "BlurNoise",
					Source = "Contrast",
					Default = 5,
				},
				BlurBrightness = InstanceInput {
					SourceOp = "BlurNoise",
					Source = "Brightness",
					Default = -1,
				},
				BlurLockXY = InstanceInput {
					SourceOp = "BlurNoise",
					Source = "LockXY",
					Default = 1,
				},
				BlurXScale = InstanceInput {
					SourceOp = "BlurNoise",
					Source = "XScale",
					Default = 1.57,
				},
				BlurYScale = InstanceInput {
					SourceOp = "BlurNoise",
					Source = "YScale",
					Default = 2,
				},
				BlurAngle = InstanceInput {
					SourceOp = "BlurNoise",
					Source = "Angle",
					Default = 0,
				},
				BlurSeethe = InstanceInput {
					SourceOp = "BlurNoise",
					Source = "Seethe",
					Default = 0,
				},
				BlurSeetheRate = InstanceInput {
					SourceOp = "BlurNoise",
					Source = "SeetheRate",
					Default = 0.11,
				},
				BlurDiscontinuous = InstanceInput {
					SourceOp = "BlurNoise",
					Source = "Discontinuous",
					Default = 0,
				},
				BlurInverted = InstanceInput {
					SourceOp = "BlurNoise",
					Source = "Inverted",
					Default = 0,
				},
				HeatDetail = InstanceInput {
					SourceOp = "FastNoise1",
					Source = "Detail",
					Page = "Heat Noise",
					Default = 4.41,
				},
				HeatContrast = InstanceInput {
					SourceOp = "FastNoise1",
					Source = "Contrast",
					Default = 1,
				},
				HeatBrightness = InstanceInput {
					SourceOp = "FastNoise1",
					Source = "Brightness",
					Default = 0,
				},
				HeatLockXY = InstanceInput {
					SourceOp = "FastNoise1",
					Source = "LockXY",
					Default = 1,
				},
				HeatXScale = InstanceInput {
					SourceOp = "FastNoise1",
					Source = "XScale",
					Default = 5.2,
				},
				HeatYScale = InstanceInput {
					SourceOp = "FastNoise1",
					Source = "YScale",
					Default = 2,
				},
				HeatAngle = InstanceInput {
					SourceOp = "FastNoise1",
					Source = "Angle",
					Default = 0,
				},
				HeatSeethe = InstanceInput {
					SourceOp = "FastNoise1",
					Source = "Seethe",
					Default = 0,
				},
				HeatSeetheRate = InstanceInput {
					SourceOp = "FastNoise1",
					Source = "SeetheRate",
					Default = 0.463,
				},
				HeatDiscontinuous = InstanceInput {
					SourceOp = "FastNoise1",
					Source = "Discontinuous",
					Default = 1,
				},
				HeatInverted = InstanceInput {
					SourceOp = "FastNoise1",
					Source = "Inverted",
					Default = 1,
				},
				Image = InstanceInput {
					SourceOp = "Scale",
					Source = "Input",
					Name = "Image",
					Page = "Heat Mask",
				},
				HeatMaskLevel = InstanceInput {
					SourceOp = "HeatMask",
					Source = "Level",
					Default = 1,
				},
				HeatMaskFilter = InstanceInput {
					SourceOp = "HeatMask",
					Source = "Filter",
				},
				HeatMaskSoftEdge = InstanceInput {
					SourceOp = "HeatMask",
					Source = "SoftEdge",
				},
				HeatMaskPasses = InstanceInput {
					SourceOp = "HeatMask",
					Source = "Passes",
					Default = 4,
				},
				HeatMaskInvert = InstanceInput {
					SourceOp = "HeatMask",
					Source = "Invert",
				},
				HeatMaskChannel = InstanceInput {
					SourceOp = "HeatMask",
					Source = "Channel",
				},
				HeatMaskLow = InstanceInput {
					SourceOp = "HeatMask",
					Source = "Low",
					ControlGroup = 38,
					Default = 0,
				},
				HeatMaskHigh = InstanceInput {
					SourceOp = "HeatMask",
					Source = "High",
					ControlGroup = 38,
					Default = 1,
				},
				HeatMaskClipBlack = InstanceInput {
					SourceOp = "HeatMask",
					Source = "ClipBlack",
					Name = "Black",
					Default = 1,
				},
				HeatMaskClipWhite = InstanceInput {
					SourceOp = "HeatMask",
					Source = "ClipWhite",
					Name = "White",
					Default = 1,
				},
				Blend = InstanceInput {
					SourceOp = "EffectMask",
					Source = "Blend",
					Page = "Common",
					Default = 1,
				},
				ProcessWhenBlendIs00 = InstanceInput {
					SourceOp = "EffectMask",
					Source = "ProcessWhenBlendIs00",
					Default = 0,
				},
				ProcessRed = InstanceInput {
					SourceOp = "EffectMask",
					Source = "ProcessRed",
					Name = "Process",
					ControlGroup = 44,
					Default = 1,
				},
				ProcessGreen = InstanceInput {
					SourceOp = "EffectMask",
					Source = "ProcessGreen",
					ControlGroup = 44,
					Default = 1,
				},
				ProcessBlue = InstanceInput {
					SourceOp = "EffectMask",
					Source = "ProcessBlue",
					ControlGroup = 44,
					Default = 1,
				},
				ProcessAlpha = InstanceInput {
					SourceOp = "EffectMask",
					Source = "ProcessAlpha",
					ControlGroup = 44,
					Default = 1,
				},
				Blank1 = InstanceInput {
					SourceOp = "EffectMask",
					Source = "Blank1",
				},
				ApplyMaskInverted = InstanceInput {
					SourceOp = "EffectMask",
					Source = "ApplyMaskInverted",
					Default = 0,
				},
				MultiplyByMask = InstanceInput {
					SourceOp = "EffectMask",
					Source = "MultiplyByMask",
					Default = 0,
				},
				FitMask = InstanceInput {
					SourceOp = "EffectMask",
					Source = "FitMask",
				},
				Blank2 = InstanceInput {
					SourceOp = "EffectMask",
					Source = "Blank2",
				},
				MaskChannel = InstanceInput {
					SourceOp = "EffectMask",
					Source = "MaskChannel",
					Default = 3,
				},
				MaskLow = InstanceInput {
					SourceOp = "EffectMask",
					Source = "MaskLow",
					ControlGroup = 51,
					Default = 0,
				},
				MaskHigh = InstanceInput {
					SourceOp = "EffectMask",
					Source = "MaskHigh",
					ControlGroup = 51,
					Default = 1,
				},
				MaskClipBlack = InstanceInput {
					SourceOp = "EffectMask",
					Source = "MaskClipBlack",
					Name = "Black",
					Default = 1,
				},
				MaskClipWhite = InstanceInput {
					SourceOp = "EffectMask",
					Source = "MaskClipWhite",
					Name = "White",
					Default = 1,
				},
				UseObject = InstanceInput {
					SourceOp = "EffectMask",
					Source = "UseObject",
					Default = 0,
				},
				UseMaterial = InstanceInput {
					SourceOp = "EffectMask",
					Source = "UseMaterial",
					Default = 0,
				},
				CorrectEdges = InstanceInput {
					SourceOp = "EffectMask",
					Source = "CorrectEdges",
				},
				ObjectID = InstanceInput {
					SourceOp = "EffectMask",
					Source = "ObjectID",
					ControlGroup = 58,
					Default = 0,
				},
				MaterialID = InstanceInput {
					SourceOp = "EffectMask",
					Source = "MaterialID",
					ControlGroup = 58,
					Default = 0,
				},
				Blank5 = InstanceInput {
					SourceOp = "EffectMask",
					Source = "Blank5",
				},
				EffectMask = InstanceInput {
					SourceOp = "EffectMask",
					Source = "EffectMask",
				}
			},
			Outputs = {
				MainOutput1 = InstanceOutput {
					SourceOp = "EffectMask",
					Source = "Output",
				}
			},
			ViewInfo = GroupInfo { Pos = { 960, 86.1515 } },
			Tools = ordered() {
				Letterbox1 = Letterbox {
					CtrlWShown = false,
					Inputs = {
						Width = Input {
							Value = 1920,
							Expression = "DepthBlur1.Input.OriginalWidth",
						},
						Height = Input {
							Value = 1080,
							Expression = "DepthBlur1.Input.OriginalHeight",
						},
						HiQOnly = Input { Value = 0, },
						Input = Input {
							SourceOp = "Blur",
							Source = "Output",
						},
					},
					ViewInfo = OperatorInfo { Pos = { 161.552, 258.49 } },
				},
				Distortion = Displace {
					CtrlWShown = false,
					Inputs = {
						Type = Input { Value = 1, },
						XOffset = Input { Value = -0.5, },
						XRefraction = Input {
							Value = 0.005,
							Expression = "(ChangeDepth1.Displace/self.Input.ProxyScale)/1000",
						},
						YOffset = Input {
							Value = -0.5,
							Expression = "XOffset",
						},
						YRefraction = Input {
							Value = 0.005,
							Expression = "XRefraction",
						},
						Input = Input {
							SourceOp = "DepthBlur1",
							Source = "Output",
						},
						Foreground = Input {
							SourceOp = "CreateBumpMap1",
							Source = "Output",
						},
					},
					ViewInfo = OperatorInfo { Pos = { 12.054, 142.132 } },
				},
				CreateBumpMap1 = CreateBumpMap {
					CtrlWShown = false,
					Inputs = {
						FilterSize = Input { Value = FuID { "5" }, },
						Input = Input {
							SourceOp = "Blur",
							Source = "Output",
						},
						ClampNormalZ = Input { Value = 0.244, },
						HeightScale = Input { Value = 10, },
					},
					ViewInfo = OperatorInfo { Pos = { 12.054, 223.111 } },
				},
				Blur = Blur {
					CtrlWShown = false,
					Inputs = {
						Filter = Input {
							Value = FuID { "Fast Gaussian" },
							Expression = "tonumber(bmd._VERSION:sub(1,2)) >= 15 and \"Fast Gaussian\" or \"Gaussian\"",
						},
						XBlurSize = Input {
							Value = 2.5,
							Expression = "ChangeDepth1.Spread/self.Input.ProxyScale",
						},
						Input = Input {
							SourceOp = "FastNoise1",
							Source = "Output",
						},
					},
					ViewInfo = OperatorInfo { Pos = { 12.054, 295.38 } },
				},
				FastNoise1 = FastNoise {
					CtrlWShown = false,
					Inputs = {
						Width = Input {
							Value = 640,
							Expression = "DepthBlur1.Input.OriginalWidth/3",
						},
						Height = Input {
							Value = 360,
							Expression = "DepthBlur1.Input.OriginalHeight/3",
						},
						["Gamut.SLogVersion"] = Input { Value = FuID { "SLog2" }, },
						Detail = Input { Value = 4.41, },
						XScale = Input { Value = 5.2, },
						SeetheRate = Input { Value = 0.463, },
						Discontinuous = Input { Value = 1, },
						Inverted = Input { Value = 1, },
						Color1Alpha = Input { Value = 1, },
						NoiseBrightnessMap = Input {
							SourceOp = "HeatMask",
							Source = "Mask",
						},
						NoiseDetailMap = Input {
							SourceOp = "HeatMask",
							Source = "Mask",
						},
						Center = Input {
							SourceOp = "Expression1_2",
							Source = "PointResult",
						},
					},
					ViewInfo = OperatorInfo { Pos = { 12.054, 352.138 } },
					UserControls = ordered() {
						Angle = {
							INP_MaxAllowed = 1000000,
							INP_Integer = false,
							INPID_InputControl = "ScrewControl",
							INP_MaxScale = 360,
							INP_Default = 0,
							INP_MinScale = 0,
							INP_MinAllowed = -1000000,
							LINKID_DataType = "Number",
							LINKS_Name = "Angle",
						},
						Center = {
							INPID_InputControl = "OffsetControl",
							LINKID_DataType = "Point",
							LINKS_Name = "Center",
						}
					}
				},
				Expression1_2 = Expression {
					CtrlWZoom = false,
					CtrlWShown = false,
					Inputs = {
						p1 = Input {
							Value = { 0.5, 0.690740740740741 },
							Expression = "ChangeDepth1.HeatDirection",
						},
						n1 = Input {
							Value = 5,
							Expression = "ChangeDepth1.Speed",
						},
						PointExpressionX = Input { Value = "if(p1x>0.5,(0.5+(time*dist(p1x,p1y,0.5,p1y))/n1),1-(0.5+(time*dist(p1x,p1y,0.5,p1y))/n1))", },
						PointExpressionY = Input { Value = "if(p1y>0.5,(0.5+(time*dist(p1x,p1y,p1x,0.5))/n1),1-(0.5+(time*dist(p1x,p1y,p1x,0.5))/n1))", },
						NumberControls = Input { Value = 1, },
						ShowNumber2 = Input { Value = 0, },
						ShowNumber3 = Input { Value = 0, },
						ShowNumber4 = Input { Value = 0, },
						ShowNumber5 = Input { Value = 0, },
						ShowNumber6 = Input { Value = 0, },
						ShowNumber7 = Input { Value = 0, },
						ShowNumber8 = Input { Value = 0, },
						ShowNumber9 = Input { Value = 0, },
						ShowPoint2 = Input { Value = 0, },
						ShowPoint3 = Input { Value = 0, },
						ShowPoint4 = Input { Value = 0, },
						ShowPoint5 = Input { Value = 0, },
						ShowPoint6 = Input { Value = 0, },
						ShowPoint7 = Input { Value = 0, },
						ShowPoint8 = Input { Value = 0, },
						ShowPoint9 = Input { Value = 0, },
					},
				},
				ChangeDepth1 = ChangeDepth {
					CtrlWShown = false,
					Inputs = {
						AnimationControl = Input { Value = 1, },
					},
					ViewInfo = OperatorInfo { Pos = { -322.224, 393.085 } },
					UserControls = ordered() {
						AnimationControl = {
							INP_Integer = false,
							LBLC_DropDownButton = true,
							LINKID_DataType = "Number",
							LBLC_NumInputs = 2,
							INPID_InputControl = "LabelControl",
							LINKS_Name = "Animation Control",
						},
						HeatDirection = {
							INP_DefaultX = 0.5,
							INPID_PreviewControl = "CrosshairControl",
							INP_DefaultY = 0.5,
							LINKID_DataType = "Point",
							INPID_InputControl = "OffsetControl",
							CHC_Style = "NormalCross",
							LINKS_Name = "Heat Direction",
						},
						Speed = {
							INP_MaxAllowed = 100,
							INP_Integer = false,
							INPID_InputControl = "SliderControl",
							INP_MaxScale = 10,
							INP_Default = 5,
							INP_MinScale = 9.99999974737875e-05,
							INP_MinAllowed = 9.99999974737875e-05,
							LINKID_DataType = "Number",
							ICD_Center = 5,
							LINKS_Name = "Density",
						},
						Display = {
							{ CCS_AddString = "Blur Noise" },
							{ CCS_AddString = "Heat Noise" },
							{ CCS_AddString = "Final " },
							INP_MaxAllowed = 1000000,
							INP_Integer = false,
							INPID_InputControl = "ComboControl",
							CC_LabelPosition = "Horizontal",
							INP_MaxScale = 1,
							INP_Default = 2,
							INP_MinScale = 0,
							INP_MinAllowed = -1000000,
							LINKID_DataType = "Number",
							LINKS_Name = "Display",
						},
						HeatBlurFilter = {
							{ CCS_AddString = "Box" },
							{ CCS_AddString = "Soften" },
							{ CCS_AddString = "SuperSoften" },
							INP_Integer = false,
							LINKID_DataType = "Number",
							CC_LabelPosition = "Horizontal",
							INPID_InputControl = "ComboControl",
							LINKS_Name = "Heat Blur Filter",
						},
						HeatBlurSize = {
							INP_Integer = false,
							LINKS_Name = "Heat Blur Size",
							INPID_InputControl = "SliderControl",
							INP_MinScale = 0,
							LINKID_DataType = "Number",
							INP_MaxScale = 20,
							INP_Default = 10,
						},
						Displace = {
							INP_MaxAllowed = 1000000,
							INP_Integer = false,
							INPID_InputControl = "SliderControl",
							INP_MaxScale = 10,
							INP_Default = 5,
							INP_MinScale = 0,
							INP_MinAllowed = -1000000,
							LINKID_DataType = "Number",
							LINKS_Name = "Displace",
						},
						Spread = {
							INP_Integer = false,
							LINKS_Name = "Spread",
							INPID_InputControl = "SliderControl",
							INP_MinScale = 0,
							LINKID_DataType = "Number",
							INP_MaxScale = 5,
							INP_Default = 2.5,
						}
					}
				},
				HeatNoiseOut = Dissolve {
					Transitions = {
						[0] = "DFTDissolve"
					},
					CtrlWShown = false,
					Inputs = {
						Blend = Input {
							Value = 0,
							Expression = "iif(ChangeDepth1.Display==1,1,0)",
						},
						Mix = Input {
							Value = 0,
							Expression = "iif(ChangeDepth1.Display==1,1,0)",
						},
						Background = Input {
							SourceOp = "BlurNoiseOut",
							Source = "Output",
						},
						Foreground = Input {
							SourceOp = "Letterbox1",
							Source = "Output",
						},
					},
					ViewInfo = OperatorInfo { Pos = { 161.552, 192.916 } },
				},
				EffectMask = Merge {
					CtrlWShown = false,
					Inputs = {
						Background = Input {
							SourceOp = "ChangeDepth1",
							Source = "Output",
						},
						Foreground = Input {
							SourceOp = "Dissolve1",
							Source = "Output",
						},
						PerformDepthMerge = Input { Value = 0, },
					},
					ViewInfo = OperatorInfo { Pos = { 321, 393.085 } },
				},
				Dissolve1 = Dissolve {
					Transitions = {
						[0] = "DFTDissolve"
					},
					CtrlWShown = false,
					Inputs = {
						Mix = Input { Expression = "iif(ChangeDepth1.Display==2,1,0)", },
						Background = Input {
							SourceOp = "HeatNoiseOut",
							Source = "Output",
						},
						Foreground = Input {
							SourceOp = "Distortion",
							Source = "Output",
						},
					},
					ViewInfo = OperatorInfo { Pos = { 321, 142.132 } },
				},
				Scale = Scale {
					CtrlWShown = false,
					Inputs = {
						XSize = Input { Value = 0.333333333333333, },
						HiQOnly = Input { Value = 0, },
						PixelAspect = Input { Value = { 1, 1 }, },
					},
					ViewInfo = OperatorInfo { Pos = { -239.62, 8.43002 } },
				},
				BlurNoiseOut = Dissolve {
					Transitions = {
						[0] = "DFTDissolve"
					},
					CtrlWShown = false,
					Inputs = {
						Blend = Input { Expression = "iif(ChangeDepth1.Display==0,1,0)", },
						Mix = Input { Expression = "iif(ChangeDepth1.Display==0,1,0)", },
						Background = Input {
							SourceOp = "Distortion",
							Source = "Output",
						},
						Foreground = Input {
							SourceOp = "Letterbox2",
							Source = "Output",
						},
					},
					ViewInfo = OperatorInfo { Pos = { 161.552, 142.132 } },
				},
				Letterbox2 = Letterbox {
					CtrlWShown = false,
					Inputs = {
						Width = Input {
							Value = 1920,
							Expression = "DepthBlur1.Input.OriginalWidth",
						},
						Height = Input {
							Value = 1080,
							Expression = "DepthBlur1.Input.OriginalHeight",
						},
						HiQOnly = Input { Value = 0, },
						Input = Input {
							SourceOp = "BrightnessContrast1",
							Source = "Output",
						},
					},
					ViewInfo = OperatorInfo { Pos = { 161.552, 78.8581 } },
				},
				BlurNoise = FastNoise {
					CtrlWShown = false,
					Inputs = {
						Width = Input {
							Value = 640,
							Expression = "DepthBlur1.Input.OriginalWidth/3",
						},
						Height = Input {
							Value = 360,
							Expression = "DepthBlur1.Input.OriginalHeight/3",
						},
						["Gamut.SLogVersion"] = Input { Value = FuID { "SLog2" }, },
						Detail = Input { Value = 5.28, },
						Contrast = Input { Value = 5, },
						Brightness = Input { Value = -1, },
						XScale = Input { Value = 1.57, },
						SeetheRate = Input { Value = 0.11, },
						Color1Alpha = Input { Value = 1, },
						NoiseBrightnessMap = Input {
							SourceOp = "HeatMask",
							Source = "Mask",
						},
						NoiseDetailMap = Input {
							SourceOp = "HeatMask",
							Source = "Mask",
						},
						Center = Input {
							Value = { 0.5, 8.16777777777778 },
							Expression = "FastNoise1.Center",
						},
					},
					ViewInfo = OperatorInfo { Pos = { 72.6262, 8.43002 } },
					UserControls = ordered() {
						Angle = {
							INP_MaxAllowed = 1000000,
							INP_Integer = false,
							INPID_InputControl = "ScrewControl",
							INP_MaxScale = 360,
							INP_Default = 0,
							INP_MinScale = 0,
							INP_MinAllowed = -1000000,
							LINKID_DataType = "Number",
							LINKS_Name = "Angle",
						},
						Center = {
							INPID_InputControl = "OffsetControl",
							LINKID_DataType = "Point",
							LINKS_Name = "Center",
						}
					}
				},
				BrightnessContrast1 = BrightnessContrast {
					CtrlWShown = false,
					Inputs = {
						ClipBlack = Input { Value = 1, },
						ClipWhite = Input { Value = 1, },
						Input = Input {
							SourceOp = "BlurNoise",
							Source = "Output",
						},
					},
					ViewInfo = OperatorInfo { Pos = { 1.12701, 51.2501 } },
				},
				DepthBlur1 = DepthBlur {
					CtrlWShown = false,
					Inputs = {
						BlurChannel = Input { Value = 5, },
						XBlurSize = Input {
							Value = 10,
							Expression = "ChangeDepth1.HeatBlurSize",
						},
						Input = Input {
							SourceOp = "ChangeDepth1",
							Source = "Output",
						},
						BlurImage = Input {
							SourceOp = "BrightnessContrast1",
							Source = "Output",
						},
					},
					ViewInfo = OperatorInfo { Pos = { -150.936, 142.132 } },
				},
				HeatMask = BitmapMask {
					CtrlWShown = false,
					Inputs = {
						ShowViewControls = Input { Value = 0, },
						Filter = Input { Value = FuID { "Fast Gaussian" }, },
						MaskWidth = Input { Value = 1920, },
						MaskHeight = Input { Value = 1080, },
						PixelAspect = Input { Value = { 1, 1 }, },
						ClippingMode = Input { Value = FuID { "None" }, },
						Image = Input {
							SourceOp = "Scale",
							Source = "Output",
						},
					},
					ViewInfo = OperatorInfo { Pos = { -128.752, 8.43002 } },
				}
			},
		}
	},
	ActiveTool = "HeatDistortion"
}