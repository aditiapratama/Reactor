--[[
v 2.4
-- update ui position on refresh button
-- add toggle layout buttons to reduce buttons count
-- replace TimeView buttom with ExpandViewer button
-- optional hide LayoutStrip on ExpandViewer press to maximize veiwer real estate (beta!)
v 2.3.2
toggle UI with launch shortcut (SHIFT+ALT+T)
v 2.3
-- add close preferences button
-- add initial window offset based on left viewer window size
-- add toggle TimeVew button
-- add optional launch on mouse position
    * if you need to launch the tool at custom position, first set prefs to launch at mouse pos, restart the script
    * then open tool preferences and set Save Position
-- add Fusion9 style View Bar with 5 simple layout presets
-- click Add Toolbars! button to launch Customize Toolbars dialogue
v 2.0 
    -- add preferences for save position and stay on top of all windows
    -- now properly working with 3D viewers
v 1.3 add some working buttons 2019-05-21
-- partial implementation for Fusion 16 by Alex Bogomolov mail@abogomolov.com
-- general discussion and screenshots: https://www.steakunderwater.com/wesuckless/viewtopic.php?p=23876#p23876
   web: https://abogomolov.com
   donate: https://paypal.me/aabogomolov
v 1.0 Initial release 2019-01-21
-- original sample script and icons by Andrew Hazelden. Thanks a lot for your invaluable help!
]]

ui = fu.UIManager
disp = bmd.UIDispatcher(ui)
comp = fu:GetCurrentComp()
fu:SetData('Toolbar16.CurrentComp', comp:GetAttrs().COMPS_FileName)
timeview_on = comp:GetData('Toolbar16.timeview_on')
if timeview_on == nil then
    timeview_on = true
end

local oldprint = print
print = function(...)
    oldprint('[Toolbar16] - ',  ...)
end

-- The app:AddConfig() command will capture the "Escape" hotkey to close the window.
app:AddConfig('ToolbarWin', {
    Target {
        ID = 'ToolbarWin',
    },
    Hotkeys {
        Target = 'ToolbarWin',
        Defaults = true,
        ESCAPE = 'Execute{cmd = [[app.UIManager:QueueEvent(obj, "Close", {})]]}',
    },
})

function _init(side)
    GlView = get_glview(side)
    viewer = GlView.CurrentViewer
    viewer_type = string.sub(tostring(viewer),1,2)
    comp = fu:GetCurrentComp()
    if not viewer then
        print('Load any 2D tool to the '.. side ..' viewer')
        return nil
    end

    guides_state = viewer:AreGuidesShown()
    controls_state = viewer:AreControlsShown()
    multiview_state = GlView:ShowingQuadView()
    locked_state = GlView:GetLocked()
    stereo_state = GlView:IsStereoEnabled()
    
    if viewer_type ~= "2D" then
        print('This tool is most useful with 2D viewers')
        return
    end

    lut_state = viewer:IsLUTEnabled()
    roi_state = viewer:IsEnableRoI()

    if fu.Version >= 16 then
        checker_state = viewer:IsCheckerEnabled()
        dod_state = viewer:IsDoDShown()
        sliders_state = viewer:IsShowGainGamma()
    else
        -- check DoD, checkers and sliders state it not implemented in Fusion 9
        dod_state = false
        checker_state = false
        sliders_state = false
    end
end


function get_glview(side)
    if side == 'left' then
        if fu.Version >= 16 then
            glview = comp:GetPreviewList().LeftView.View
        else
            glview = comp:GetPreviewList().Left.View
        end
    elseif side == 'right' then
        if fu.Version >= 16 then
            glview = comp:GetPreviewList().RightView.View
        else
            glview = comp:GetPreviewList().Right.View
        end
    end
    return glview
end

function get_state(value)
    local data = fu:GetData(value)
    if data and data == 'true' then
        return true
    end
    return false
end

function get_tb_offset(timeview_on)
    TBoffset = 35 -- fusion 9 offset
    if fu.Version >= 16 then
        if fu:GetPrefs('Global.Unsorted.ToolbarState') == false then
            TBoffset = 85 -- fusion 16 offset
        else
            TBoffset = 136 -- fusion 16 offset with default toolbar turned on
        end
        if timeview_on == false then
            -- timeView is off, offset is zero
            TBoffset = TBoffset - 84
        end
    end
    return TBoffset
end

function get_window_xy()
    local view_attrs = get_glview('left'):GetAttrs()
    main_window_dimensions = fusion:GetPrefs("Global.Main.Window")
    if not main_window_dimensions or main_window_dimensions.Width == -1 then
        if app:GetVersion().App == 'Fusion' then
            print("[Warning] The Window dimensions are undefined. Please press 'Grab probram layout' button in the Layout Preferences section.")
            app:ShowPrefs("PrefsLayout")
        else
            print('setting UI width to default 1920px until better solution for Resolve arrived')
            main_window_dimensions.Width = 1920
        end
    end
    local savepos_state = get_state('Toolbar16.SavePos')
    local get_pos = fu:GetData('Toolbar16.Position')
    local on_mouse = get_state('Toolbar16.OnMouse')
    if get_pos and savepos_state then
        print('restoring position')
        return get_pos[1], get_pos[2]
    elseif on_mouse then
        print('launching at mouse position')
        return fu.MouseX, fu.MouseY
    else
        local leftOffset = main_window_dimensions.Width*.12
        posX = main_window_dimensions.Width / 2 - leftOffset
        posY = view_attrs.VIEWN_Bottom + get_tb_offset(timeview_on)
        if posY > main_window_dimensions.Height then
            posY = main_window_dimensions.Height - 10
        end
        return posX, posY
    end
end

function show_ui()
    show_on_top = get_state('Toolbar16.OnTop')
    _init('left')
    -- check if window exists
    width, height = 770,26
    iconsMedium = {16,26}
    iconsMediumLong = {34,26}
    x, y = get_window_xy()
    win = disp:AddWindow({
        ID = 'ToolbarWin',
        TargetID = 'ToolbarWin',
        WindowTitle = 'Viewer Toolbar for Fusion16',
        WindowFlags = {SplashScreen = true, NoDropShadowWindowHint = false, WindowStaysOnTopHint = show_on_top},
        Geometry = {x - (width) / 2, y, width, height},
        Spacing = 0,
        Margin = 0,
        
        ui:VGroup{
            ID = 'root',
            -- Add your GUI elements here:
            ui:HGroup{
                ui:HGroup{
                    Weight = 0.8,
                    ui:HGap(0.25,0),
                    ui:Button{
                        ID = 'IconButtonGuides',
                        Text = '',
                        Flat = true,
                        IconSize = {16,16},
                        Icon = ui:Icon{File = 'Scripts:/Comp/Toolbar16/Icons/PT_Guides.png'},
                        MinimumSize = iconsMedium,
                        Checkable = true,
                        Checked = guides_state 
                    },
                    ui:Button{
                        ID = 'IconButtonZoom',
                        Text = '100%',
                        Flat = true,
                        IconSize = {6,10},
                        MinimumSize = iconsMediumLong,
                        Checkable = false,
                    },
                    ui:Button{
                        ID = 'IconButtonFit',
                        Text = 'Fit',
                        IconSize = {6,2},
                        Flat = true,
                        MinimumSize = {28,26},
                        Checkable = false,
                    },
                    ui:Button{
                        ID = 'IconButtonPolyline',
                        Flat = true,
                        IconSize = {16,16},
                        MinimumSize = iconsMedium,
                        Checkable = false,
                        Icon = ui:Icon{File = 'Scripts:/Comp/Toolbar16/Icons/PT_Polyline.png'},
                    },
                    ui:Button{
                        ID = 'IconButtonBSpline',
                        Flat = true,
                        IconSize = {16,16},
                        Icon = ui:Icon{File = 'Scripts:/Comp/Toolbar16/Icons/PT_BSpline.png'},
                        MinimumSize = iconsMedium,
                        Checkable = false,
                    },
                    ui:Button{
                        ID = 'IconButtonBitmap',
                        Flat = true,
                        IconSize = {16,16},
                        Icon = ui:Icon{File = 'Scripts:/Comp/Toolbar16/Icons/PT_Bitmap.png'},
                        MinimumSize = iconsMedium,
                        Checkable = false,
                    },
                    ui:Button{
                        ID = 'IconButtonPaint',
                        Flat = true,
                        IconSize = {16,16},
                        Icon = ui:Icon{File = 'Scripts:/Comp/Toolbar16/Icons/PT_Paint.png'},
                        MinimumSize = iconsMedium,
                        Checkable = false,
                    },
                    ui:Button{
                        ID = 'IconButtonWand',
                        Flat = true,
                        IconSize = {16,16},
                        Icon = ui:Icon{File = 'Scripts:/Comp/Toolbar16/Icons/PT_Wand.png'},
                        MinimumSize = iconsMedium,
                        Checkable = false,
                    },
                    ui:Button{
                        ID = 'IconButtonRectangle',
                        Flat = true,
                        IconSize = {16,16},
                        Icon = ui:Icon{File = 'Scripts:/Comp/Toolbar16/Icons/PT_Rectangle.png'},
                        MinimumSize = iconsMedium,
                        Checkable = false,
                    },
                    ui:Button{
                        ID = 'IconButtonCircle',
                        Flat = true,
                        IconSize = {16,16},
                        Icon = ui:Icon{File = 'Scripts:/Comp/Toolbar16/Icons/PT_Circle.png'},
                        MinimumSize = iconsMedium,
                        Checkable = false,
                    },
                    ui:Button{
                        ID = 'IconButtonStereo',
                        Flat = true,
                        Text = '',
                        IconSize = {16,16},
                        Icon = ui:Icon{File = 'Scripts:/Comp/Toolbar16/Icons/PT_Stereo.png'},
                        MinimumSize = iconsMedium,
                        Checkable = true,
                        Checked = stereo_state 
                    },
                    ui:Button{
                        ID = 'IconButtonLUT',
                        Text = 'LUT',
                        Flat = true,
                        MinimumSize = {30,16},
                        Checkable = true,
                        Checked = lut_state
                    },
                    ui:Button{
                        ID = 'IconButtonROI',
                        Text = 'RoI',
                        Flat = true,
                        MinimumSize = {30,16},
                        Checkable = true,
                        Checked = roi_state 
                    },
                    ui:Button{
                        ID = 'IconButtonDoD',
                        Text = 'DoD',
                        Flat = true,
                        IconSize = {5,10},
                        MinimumSize = iconsMediumLong,
                        Checkable = true,
                        Checked = dod_state
                    },
                    ui:Button{
                        ID = 'IconButtonLockCold',
                        Flat = true,
                        Text = '',
                        IconSize = {16,16},
                        Icon = ui:Icon{File = 'Scripts:/Comp/Toolbar16/Icons/PT_LockCold.png'},
                        MinimumSize = iconsMedium,
                        Checkable = true,
                        Checked = locked_state
                    },
                    ui:Button{
                        ID = 'IconButtonControls',
                        Flat = true,
                        Text = '',
                        IconSize = {16,16},
                        Icon = ui:Icon{File = 'Scripts:/Comp/Toolbar16/Icons/PT_Controls.png'},
                        MinimumSize = iconsMedium,
                        Checkable = true,
                        Checked = controls_state
                    },
                    ui:Button{
                        ID = 'IconButtonChequers',
                        Flat = true,
                        Text = '',
                        IconSize = {16,16},
                        Icon = ui:Icon{File = 'Scripts:/Comp/Toolbar16/Icons/PT_Chequers.png'},
                        MinimumSize = iconsMedium,
                        Checkable = true,
                        Checked = checker_state
                    },
                    -- ui:Button{
                    --     ID = 'IconButtonOne2One',
                    --     Flat = true,
                    --     IconSize = {16,16},
                    --     Icon = ui:Icon{File = 'Scripts:/Comp/Toolbar16/Icons/PT_One2One.png'},
                    --     MinimumSize = iconsMedium,
                    --     Checkable = true,
                    -- },
                    -- ui:Button{
                    --     ID = 'IconButtonNormalise',
                    --     Flat = true,
                    --     IconSize = {16,16},
                    --     Icon = ui:Icon{File = 'Scripts:/Comp/Toolbar16/Icons/PT_Normalise.png'},
                    --     MinimumSize = iconsMedium,
                    --     Checkable = true,
                    -- },
                    ui:Button{
                        ID = 'IconButtonSliders',
                        Flat = true,
                        Text = '',
                        IconSize = {16,16},
                        Icon = ui:Icon{File = 'Scripts:/Comp/Toolbar16/Icons/PT_Sliders.png'},
                        MinimumSize = iconsMedium,
                        Checkable = true,
                        Checked = sliders_state 
                    },
                    ui:Button{
                        ID = 'IconButtonMultiView',
                        Text = '',
                        Flat = true,
                        IconSize = {16,16},
                        Icon = ui:Icon{File = 'Scripts:/Comp/Toolbar16/Icons/PT_MultiView.png'},
                        MinimumSize = iconsMedium,
                        Checkable = true,
                        Checked = multiview_state 
                    },
                },
                ui:HGroup{
                    Weight = 0.2,
                    ui:Button{
                        ID = 'Left',
                        Text = 'left',
                        IconSize = {6,2},
                        Flat = true,
                        MinimumSize = iconsMediumLong,
                        Checkable = true,
                        Checked = true,
                    },
                    ui:Button{
                        ID = 'Right',
                        Text = 'right',
                        Flat = true,
                        MinimumSize = iconsMediumLong,
                        IconSize = {6,2},
                        Checkable = true,
                        Checked = false,
                    },
                    ui:Button{
                        ID = 'RefreshButtons',
                        Text = '',
                        IconSize = {12,12},
                        MinimumSize = iconsMedium,
                        Icon = ui:Icon{File = 'Scripts:/Comp/Toolbar16/Icons/refresh_icon.png'},
                        Enable = false,
                    },                    
                    ui:Button{
                        ID = 'Layout01',
                        Flat = true,
                        IconSize = {16,16},
                        MinimumSize = iconsMedium,
                        Checkable = false,
                        Icon = ui:Icon{File = 'Scripts:/Comp/Toolbar16/Icons/PT_Layout01.png'},
                    },                    
                    ui:Button{
                        ID = 'Layout02',
                        Flat = true,
                        IconSize = {16,16},
                        MinimumSize = iconsMedium,
                        Checkable = false,
                        Icon = ui:Icon{File = 'Scripts:/Comp/Toolbar16/Icons/PT_Layout02.png'},
                    },                    
                    ui:Button{
                        ID = 'Layout03',
                        Flat = true,
                        IconSize = {16,16},
                        MinimumSize = iconsMedium,
                        Checkable = false,
                        Icon = ui:Icon{File = 'Scripts:/Comp/Toolbar16/Icons/PT_Layout03.png'},
                    },                    
                    ui:Button{
                        ID = 'ExpandViewButton',
                        Flat = true,
                        IconSize = {16,16},
                        MinimumSize = iconsMedium,
                        Checkable = false,
                        Icon = ui:Icon{File = 'Scripts:/Comp/Toolbar16/Icons/PT_Expand.png'},
                    },                             
                    ui:Button{
                        ID = 'CloseButton',
                        Text = 'Exit',
                        Flat = false,
                        MinimumSize = {40,16},
                        Checkable = false
                    },
                    ui:Button{
                        ID = 'LaunchPrefs',
                        Text = '',
                        Flat = true,
                        IconSize = {6,6},
                        Icon = ui:Icon{File = 'Scripts:/Comp/Toolbar16/Icons/PT_Triangle.png'},
                        MinimumSize = iconsMedium,
                        Checkable = false,
                    },
                    ui.HGap(0.25,0),
                },
            },

        },
    })
    return win, {x, y}
end

local main_win = ui:FindWindow("ToolbarWin")
if main_win then
    print('toggle existing UI') 
    if main_win.Hidden then
        main_win:SetHidden(false)
    else
        main_win:SetHidden(true)
    end
    return
else
    win, position = show_ui()
    fu:SetData('Toolbar16.Position', position)
end

-- The window was closed
function win.On.ToolbarWin.Close(ev)
    disp:ExitLoop()
end

function win.On.CloseButton.Clicked(ev)
    disp:ExitLoop()
end

-- show preferences

local prefs_dlg = nil

function toggle_prefs()
    if prefs_dlg then
        prefs_dlg:Hide()
        prefs_dlg = nil
    else
        show_prefs_window(position)
    end
end


function show_prefs_window(pos)
    local prefx, prefy = pos[1], pos[2]
    local offsetY = -125
    if prefy < 120 then
        offsetY = offsetY + 120
    end
    savepos_state = get_state('Toolbar16.SavePos')
    ontop_state = get_state('Toolbar16.OnTop')
    on_mouse_pos = get_state('Toolbar16.OnMouse')
    expand_with_layout = get_state('Toolbar16.ExpandWithLayout')

    prefs_dlg = disp:AddWindow({
        ID = 'TBPrefs',
        TargetID = 'TBPrefs',
        WindowFlags = {SplashScreen = true, NoDropShadowWindowHint = true, WindowStaysOnTopHint = false},
        Geometry = {prefx +135, prefy+offsetY, 250, 125},
            ui:VGroup{
            ID = 'prefs',
                ui:HGroup{
                    Weight = 0.5,
                    ui:CheckBox{
                        ID = 'SavePos',
                        Text = 'save position',
                        Checked = savepos_state,
                    },
                    ui:Button{
                        ID = 'FlushData',
                        Text = 'reset prefs',
                        MinimumSize = {5,10},
                    },
                },
                ui:HGroup{
                    ui:CheckBox{
                        ID = 'OnMouse',
                        Text = 'launch at mouse',
                        Checked = on_mouse_pos,
                    },
                        ui:CheckBox{
                        ID = 'OnTop',
                        Text = 'stay on top',
                        Checked = ontop_state,
                    },
                },
                ui:HGroup{
                    ui:CheckBox{
                        ID = 'ExpandWithLayout',
                        Text = 'hide LayoutStrip (Beta)',
                        Checked = expand_with_layout,
                    },
                },
                ui:HGroup{
                    ui:Button{
                        MaximumSize = {150, 20},
                        Weight = .3,
                        ID = 'MoreToolbars',
                        Text = 'add toolbars!',
                    },
                    ui:Button{
                        Weight = .7,
                        MaximumSize = {100, 20},
                        ID = 'ClosePrefs',
                        Text = 'close',
                    },
                }
            },
        })

    pref_itm = prefs_dlg:GetItems()

    function prefs_dlg.On.ClosePrefs.Clicked(ev)
        prefs_dlg:Hide()
        prefs_dlg = nil
    end

    function prefs_dlg.On.MoreToolbars.Clicked(ev)
        fu:CustomizeToolbars()
        prefs_dlg:Hide()
        prefs_dlg = nil
    end

    function prefs_dlg.On.OnMouse.Clicked(ev)
        local mousePos = pref_itm.OnMouse.Checked
        if mousePos then
            pref_itm.SavePos.Checked = false
            print('next time UI will open at mouse position')
        else
            print('UI will lauch normally under the viewer')
        end
        fu:SetData('Toolbar16.OnMouse', tostring(mousePos))
    end

    function prefs_dlg.On.ExpandWithLayout.Clicked(ev)
        local expand_status = tostring(pref_itm.ExpandWithLayout.Checked)
        fu:SetData('Toolbar16.ExpandWithLayout', expand_status)
        print('hide LayoutStrip is set to ' .. expand_status)
    end

    function prefs_dlg.On.SavePos.Clicked(ev)
        local savePos = pref_itm.SavePos.Checked
        if savePos then
            pref_itm.OnMouse.Checked = false
            fu:SetData('Toolbar16.OnMouse', false)
        end
        fu:SetData('Toolbar16.SavePos', tostring(savePos))
        print('saving main window position set to ' .. tostring(savePos))
    end

    function prefs_dlg.On.OnTop.Clicked(ev)
        local onTop = pref_itm.OnTop.Checked
        print('stay on top is set to ' .. tostring(onTop))
        fu:SetData('Toolbar16.OnTop', tostring(onTop))
    end

    function prefs_dlg.On.FlushData.Clicked(ev)
        fu:SetData('Toolbar16')
        print('toolbar preferences data flushed')
        prefs_dlg:Hide()
        prefs_dlg = nil
    end

    prefs_dlg:Show()
end


itm = win:GetItems()


function win.On.LaunchPrefs.Clicked(ev)
    pref_win = ui:FindWindow('TBPrefs')
    toggle_prefs()
end


function refresh_ui()
    itm.IconButtonStereo.Checked = stereo_state
    itm.IconButtonLUT.Checked = lut_state
    itm.IconButtonROI.Checked = roi_state
    itm.IconButtonDoD.Checked = dod_state
    itm.IconButtonLockCold.Checked = locked_state
    itm.IconButtonControls.Checked = controls_state 
    itm.IconButtonChequers.Checked = checker_state
    itm.IconButtonSliders.Checked = sliders_state
    itm.IconButtonGuides.Checked = guides_state
    itm.IconButtonMultiView.Checked = multiview_state
end


function win.On.Right.Clicked(ev)
    itm.Left.Checked = false
    _init('right')
    refresh_ui()
end

function win.On.Left.Clicked(ev)
    itm.Right.Checked = false
    _init('left')
    refresh_ui()
end

---------- set glview attrs

function win.On.IconButtonMultiView.Clicked(ev)
    state = itm.IconButtonMultiView.Checked
    print('[Guides] [Button State] ' , state)
    GlView:ShowQuadView(state)
end

function win.On.IconButtonZoom.Clicked(ev)
    state = itm.IconButtonZoom.Checked
    print('[Zoom] is set to 100%')
    GlView:SetScale(1)
end

function win.On.IconButtonFit.Clicked(ev)
    state = itm.IconButtonFit.Checked
    print('[Fit] to GlView')
    GlView:SetScale(0)
end

function win.On.IconButtonStereo.Clicked(ev)
    state = itm.IconButtonStereo.Checked
    print('[Stereo][Button State] ', state)
    GlView:EnableStereo()
end

function win.On.IconButtonLockCold.Clicked(ev)
    state = itm.IconButtonLockCold.Checked
    print('[LockCold][Button State] ', state)
    GlView:SetLocked(state)
end

---------- add tools 
function win.On.IconButtonPolyline.Clicked(ev)
    print('[Polyline] created')
    comp:AddTool('PolylineMask', -32768, -32768)
end

function win.On.IconButtonBSpline.Clicked(ev)
    print('[BSpline] created')
    comp:AddTool('BSplineMask', -32768, -32768)
end

function win.On.IconButtonBitmap.Clicked(ev)
    print('[Bitmap] created')
    comp:AddTool('BitmapMask', -32768, -32768)
end

function win.On.IconButtonPaint.Clicked(ev)
    print('[Paint] created')
    comp:AddTool('PaintMask', -32768, -32768)
end

function win.On.IconButtonWand.Clicked(ev)
    print('[Wand] created')
    comp:AddTool('WandMask', -32768, -32768)
end

function win.On.IconButtonRectangle.Clicked(ev)
    print('[Rectangle] created')
    comp:AddTool('RectangleMask', -32768, -32768)
end

function win.On.IconButtonCircle.Clicked(ev)
    print('[Circle] created')
    comp:AddTool('EllipseMask', -32768, -32768)
end

------- change GlView attrs

function win.On.IconButtonGuides.Clicked(ev)
    state = itm.IconButtonGuides.Checked
    viewer = GlView.CurrentViewer
    if not viewer then
        return
    end
    viewer:ShowGuides(state)
    viewer:Redraw()
    print('[Guides] [Button state] ', state)
end

function win.On.IconButtonLUT.Clicked(ev)
    state = itm.IconButtonLUT.Checked
    viewer = GlView.CurrentViewer
    if not viewer or viewer_type == '3D' then
        return
    end
    print('[LUT][Button State] ', state)
    viewer:EnableLUT(state)
    viewer:Redraw()
end

function win.On.IconButtonROI.Clicked(ev)
    state = itm.IconButtonROI.Checked
    viewer = GlView.CurrentViewer
    if not viewer or viewer_type == '3D' then
        return
    end
    print('[ROI][Button State] ', state)
    viewer:EnableRoI(state)
    viewer:Redraw()
end

function win.On.IconButtonDoD.Clicked(ev)
    state = itm.IconButtonDoD.Checked
    viewer = GlView.CurrentViewer
    if not viewer or viewer_type == '3D' then
        return
    end
    print('[DoD][Button State] ', state)
    viewer:ShowDoD(state)
    viewer:Redraw()
end

function win.On.IconButtonControls.Clicked(ev)
    state = itm.IconButtonControls.Checked
    viewer = GlView.CurrentViewer
    if not viewer then
        return
    end
    print('[Controls][Button State] ', state)
    viewer:ShowControls(state)
    viewer:Redraw()
end

function win.On.IconButtonSliders.Clicked(ev)
    state = itm.IconButtonSliders.Checked
    if fu.Version >= 16 then
        viewer = GlView.CurrentViewer
        if not viewer or viewer_type == '3D' then
            return
        end
        itm.IconButtonControls.Checked = true
        viewer:ShowControls(true)
        viewer:ShowGainGamma(state)
        print('[Sliders][Button State] ', state)
    else
        print('this function does not work in Fu9')
    end
end

function win.On.IconButtonChequers.Clicked(ev)
    state = itm.IconButtonChequers.Checked
    if fu.Version >= 16 then
        viewer = GlView.CurrentViewer
        if not viewer or viewer_type == '3D' then
            return
        end
        viewer:EnableChecker(state)
        viewer:Redraw()
        print('[Chequers][Button State] ', state)
    else
        print('this function does not work in Fu9')
    end
end

function move_window(main_win, offset)
    local view_attrs = get_glview('left'):GetAttrs()
    if not main_win then
        main_win = ui:FindWindow('ToolbarWin')
    end
    local window_ypos = main_win:Y()
    local viewer_Y = view_attrs.VIEWN_Bottom
    new_pos = viewer_Y + offset
    if new_pos ~= window_ypos then
        if new_pos > main_window_dimensions.Height then
            new_pos = main_window_dimensions.Height - 10
        end
        -- print("moving window")
        main_win:Move({main_win:X(), new_pos})
        position = {position[1], new_pos}
    end
end


function win.On.RefreshButtons.Clicked(ev)
    if itm.Left.Checked == true then
        side = 'left'
    else
        side = 'right'
    end
    _init(side)
    _init(side)
    refresh_ui()
    current_comp_name = comp:GetAttrs().COMPS_FileName 
    if current_comp_name ~= fu:GetData('Toolbar16.CurrentComp') then
        fu:SetData('Toolbar16.CurrentComp', current_comp_name)
        comp:DoAction("Fusion_View_Show", {view = "Time", show = true})
    end
    timeview_on = comp:GetData('Toolbar16.timeview_on')
    offset = get_tb_offset(timeview_on)
    move_window(win, offset)
end

-- Layout change

function win.On.Layout01.Clicked(ev)
    comp:DoAction("Fusion_View_Show", {view = "Viewer2"})
    comp:DoAction("Fusion_View_Show", {view = "Viewer1", show = true})
    comp:DoAction("Fusion_View_Show", {view = "Inspector", show = true})
    comp:DoAction("Fusion_Zone_Expand", {zone = "Right", expand = true})
    if inspector_hidden then
        win:Move({win:X() - 200, win:Y()})
        inspector_hidden = false
    end
end

function win.On.Layout02.Clicked(ev)
    comp:DoAction("Fusion_View_Show", {view = "Viewer2"})
    comp:DoAction("Fusion_View_Show", {view = "Inspector", show = true})
    comp:DoAction("Fusion_Zone_Expand", {zone = "Right", expand = false}) 
    if inspector_hidden then
        win:Move({win:X() - 200, win:Y()})
        inspector_hidden = false
    end
end

function win.On.Layout03.Clicked(ev)
    comp:DoAction("Fusion_View_Show", {view = "Viewer2"})
    comp:DoAction("Fusion_View_Show", {view = "Viewer1", show = true})
    comp:DoAction("Fusion_View_Show", {view = "Inspector", show = false})
    comp:DoAction("Fusion_Zone_Expand", {zone = "Right", expand = false})
    if not inspector_hidden then
        win:Move({win:X() + 200, win:Y()})
        inspector_hidden = true
    end
end

function win.On.ExpandViewButton.Clicked(ev)
    timeview_on = comp:GetData('Toolbar16.timeview_on')
    if timeview_on == nil then
        timeview_on = true
    end
    timeview_on = not timeview_on
    comp:SetData("Toolbar16.timeview_on", timeview_on)
    expand_with_layout = get_state('Toolbar16.ExpandWithLayout')
    if expand_with_layout then
        _ViewLayout = comp.CurrentFrame:GetViewLayout()
        _LayoutStripState = _ViewLayout.ViewInfo.LayoutStrip.Show 
        _ViewLayout.ViewInfo.LayoutStrip.Show = timeview_on 
        comp.CurrentFrame:SetViewLayout(_ViewLayout)
    end
    comp:DoAction("Fusion_View_Show", {view = "Time", show = timeview_on})
    local offset = get_tb_offset(timeview_on)
    move_window(win, offset)
end

-- Display the window
win:Show()

-- Keep the window updating until the script is quit
disp:RunLoop()
win:Hide()
app:RemoveConfig('ToolbarWin')
collectgarbage()
print('[Done]')
